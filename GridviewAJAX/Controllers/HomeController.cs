﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GridviewAJAX.Helper;
using GridviewAJAX.Models;
using Microsoft.AspNetCore.Mvc;

namespace GridviewAJAX.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            //List<Info> info = new List<Info>();
            //info.Add(new Info { Id = 1, Name = "User1" });
            //info.Add(new Info { Id = 2, Name = "User2" });
            //info.Add(new Info { Id = 3, Name = "User3" });
            return View();
        }

        public IActionResult GetPaggedData(int pageNumber = 1, int pageSize = 10)
        {
            List<Info> info = new List<Info>();
            for (int i = 1; i <= 50; i++)
            {
                info.Add(new Info { Id = i });
            }

            var pagedData = Pagination.PagedResult(info, pageNumber, pageSize);
            return Json(pagedData);
            //List<Info> ObjInfo = new List<Info>();
            //Info Obj = new Info( {Id: } );
        }
    }
}