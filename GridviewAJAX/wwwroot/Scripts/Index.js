﻿$(document).ready(function () {
    //Initially load pagenumber=1
    GetPageData(1);
    
});


function GetPageData(pageNum, pageSize) {
    //After every trigger remove previous data and paging
    $("#tblData").empty();
    $("#paged").empty();
    $.ajax({
        dataType: "json",
        url: "/Home/GetPaggedData",
        data: { pageNumber: pageNum, pageSize: pageSize },
        success: function (response) {
            var rowData = "";
            for (var i = 0; i < response.data.length; i++) {
                rowData = rowData + '<tr><td>' + response.data[i].id + '</td></tr>';
            }
            $("#tblData").append(rowData);

            PaggingTemplate(response.totalPages, response.currentPage);

            //document.getElementById(pageNum).style.color = 'red';
            //Use code below or above have the same result!!!
            $("#" + pageNum).css('color', 'red');
        }
    });
    
}

function PaggingTemplate(totalPage, currentPage) {
    var template = "";
    var TotalPages = totalPage;
    var CurrentPage = currentPage;
    
    template = "<p>" + CurrentPage + " of " + TotalPages + " pages</p>"
    template = template + '<ul class="pager">'
    for (var i = 1; i <= TotalPages; i++) {
        template = template + '<li><button id="'+ i +'" onclick="GetPageData(' + i + ')">' + i + '</button>';
    }

    $("#paged").append(template);
}
